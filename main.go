package main

import (
	"github.com/Bios-Marcel/tview"
	"github.com/diamondburned/librespot-golang/librespot"
	"github.com/diamondburned/librespot-golang/librespot/core"
	"github.com/gdamore/tcell"
	"gitlab.com/diamondburned/spotuify/views/loading"
	"gitlab.com/diamondburned/spotuify/views/login"
)

var (
	app *tview.Application
	s   *core.Session
)

func init() {
	// Initializing clean borders
	tview.Borders.HorizontalFocus = tview.Borders.Horizontal
	tview.Borders.VerticalFocus = tview.Borders.Vertical

	tview.Borders.TopLeftFocus = tview.Borders.TopLeft
	tview.Borders.TopRightFocus = tview.Borders.TopRight
	tview.Borders.BottomLeftFocus = tview.Borders.BottomLeft
	tview.Borders.BottomRightFocus = tview.Borders.BottomRight
}

func main() {
	app = tview.NewApplication()
	defer app.Stop()

	app.SetBeforeDrawFunc(func(s tcell.Screen) bool {
		s.Clear()
		return false
	})

	// nil for callbacks
	l, f := login.NewLogin(nil)
	login.ChangeSubmitFn(func(us, pw string) {
		app.QueueUpdateDraw(func() {
			app.SetRoot(loading.NewLoading("Loading"), true)
		})

		session, err := librespot.Login(us, pw, "Spotuify")
		if err != nil {
			f.SetBorderColor(tcell.ColorRed)

			// Oh, how fun callbacks are.
			app.QueueUpdateDraw(func() {
				app.SetRoot(l, true)
			})

			return
		}

		loginCallback(session)
	})

	if err := app.SetRoot(l, true).Run(); err != nil {
		panic(err)
	}
}
