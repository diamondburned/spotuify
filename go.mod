module gitlab.com/diamondburned/spotuify

require (
	github.com/Bios-Marcel/tview v0.0.0-20190405085044-a91fccfc5cec
	github.com/atotto/clipboard v0.1.2
	github.com/diamondburned/librespot-golang v0.0.0-20190420082447-cc934897aecc
	github.com/diamondburned/tcell v1.1.6
	github.com/diamondburned/tview v0.0.0-20190404043148-f15a1bbd5aa1
	github.com/gdamore/tcell v1.1.1
	github.com/rivo/tview v0.0.0-20190406182340-90b4da1bd64c
	gitlab.com/diamondburned/tview-sixel v0.0.0-20190419130434-837e5c4cfb49
)
