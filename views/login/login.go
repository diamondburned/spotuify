package login

import (
	"github.com/atotto/clipboard"
	"github.com/gdamore/tcell"
	"github.com/Bios-Marcel/tview"
)

var submitFn func(string, string)

// NewLogin creates a log-in view
func NewLogin(submit func(username, password string)) (tview.Primitive, *tview.Form) {
	form := tview.NewForm()
	form.SetBackgroundColor(-1)
	form.SetItemPadding(1)
	form.SetFieldBackgroundColor(-1)
	form.SetLabelColor(tcell.ColorWhite)
	form.SetButtonBackgroundColor(tcell.Color35)
	form.SetBorder(true)
	form.SetBorderPadding(1, 1, 2, 2)
	form.SetBorderColor(tcell.Color35)

	us := tview.NewInputField()
	pw := tview.NewInputField().SetMaskCharacter('*')

	InputClipboard(us)
	InputClipboard(pw)

	us.SetLabel("Username")
	pw.SetLabel("Password")

	us.SetFieldWidth(45)
	pw.SetFieldWidth(45)

	form.AddFormItem(us)
	form.AddFormItem(pw)

	submitFn = submit

	form.AddButton("Login", func() {
		go submitFn(us.GetText(), pw.GetText())
	})

	vFlex := tview.NewFlex()

	hFlex := tview.NewFlex().SetDirection(tview.FlexRow)
	hFlex.AddItem(tview.NewBox(), 0, 1, false)
	hFlex.AddItem(form, 9, 1, true)
	hFlex.AddItem(tview.NewBox(), 0, 1, false)

	vFlex.AddItem(tview.NewBox(), 0, 1, false)
	vFlex.AddItem(hFlex, 0, 2, true)
	vFlex.AddItem(tview.NewBox(), 0, 1, false)

	return vFlex, form
}

// ChangeSubmitFn changes the submit function
func ChangeSubmitFn(s func(username, password string)) {
	submitFn = s
}

// InputClipboard sets the handler to support clipboard with Ctrl V
func InputClipboard(i *tview.InputField) {
	i.SetInputCapture(func(ev *tcell.EventKey) *tcell.EventKey {
		if ev.Key() == tcell.KeyCtrlV {
			c, err := clipboard.ReadAll()
			if err != nil {
				return nil
			}

			i.SetText(i.GetText() + c)
			return nil
		}

		return ev
	})
}
