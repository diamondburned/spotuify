package layout

import (
	"github.com/Bios-Marcel/tview"
	"github.com/gdamore/tcell"
)

var (
	// Grid is the global layout
	Grid *tview.Grid

	// Decoration is the top bar, containing search
	// and whatever
	Decoration *tview.Flex

	// Search is the search field for the grid
	Search *tview.InputField

	// Statusbar is the flex for the bottom
	Statusbar *tview.Flex

	// Sidebar is the sidebar, hidden when the
	// terminal is too small
	Sidebar *tview.List

	// Carousel is the main display
	Carousel *tview.Flex
)

// NewLayout makes a new grid layout
func NewLayout() *tview.Grid {
	Grid = tview.NewGrid()
	Grid.SetBorders(false)
	Grid.SetRows(3, 0, 4)
	Grid.SetColumns(30, 0)

	Search = tview.NewInputField()
	Search.SetFieldBackgroundColor(-1)
	Search.SetPlaceholder("Search...")
	Search.SetPlaceholderTextColor(tcell.ColorWhite)
	Search.SetBorder(false)

	Decoration = tview.NewFlex()
	Decoration.AddItem(Search, 40, 0, false)

	decoration := tview.NewFrame(Decoration)
	decoration.SetBorders(1, 0, 0, 0, 1, 1)
	decoration.SetBorder(false)

	Statusbar = tview.NewFlex()
	Statusbar.SetBorder(true)
	Statusbar.SetTitleAlign(tview.AlignCenter)
	Statusbar.SetDirection(tview.FlexRow)

	Sidebar = tview.NewList()
	Sidebar.ShowSecondaryText(false)
	Sidebar.SetHighlightFullLine(true)
	Sidebar.AddItem(" Home", "", 0, nil)
	Sidebar.AddItem(" Songs", "", 0, nil)
	Sidebar.AddItem(" Albums", "", 0, nil)
	Sidebar.AddItem(" Artists", "", 0, nil)
	Sidebar.AddItem(" Playlists", "", 0, nil)

	sidebar := tview.NewFrame(Sidebar)
	sidebar.SetBorders(0, 1, 0, 0, 1, 2)

	Carousel = tview.NewFlex()
	Carousel.AddItem(
		tview.NewTextView().SetText("Coming soon!"),
		0, 1, false,
	)

	carousel := tview.NewFrame(Carousel)
	carousel.SetBorders(0, 1, 0, 0, 1, 1)

	// Top and bottom views, search and statusbar

	/*
		Explaining these numbers:
		1. (0|2) are the row numbers, Search at the top (0),
		   statusbar at the bottom (2)
		2. 0 is the column number, since the top and bottom
		   only has one column, it's 0.
		3. 1 is the row span. Since in each row, there is only
		   one item (unlike the main row having sidebar AND
		   carousel), it's 1.
		4. 2 is the column span. Since we have a total of 2
		   columns as declared in `SetColumns(n, n)`, the
		   maximum is 2.
	*/
	Grid.AddItem(decoration, 0, 0, 1, 2, 0, 0, false)
	Grid.AddItem(Statusbar, 2, 0, 1, 2, 0, 0, false)

	// Middle row, main view

	// Mini UI
	Grid.AddItem(sidebar, 0, 0, 0, 0, 0, 0, false)
	Grid.AddItem(carousel, 1, 0, 1, 2, 0, 0, false)

	// At least 80 columns, full UI
	Grid.AddItem(sidebar, 1, 0, 1, 1, 0, 80, false)
	Grid.AddItem(carousel, 1, 1, 1, 1, 0, 80, false)

	return Grid
}
