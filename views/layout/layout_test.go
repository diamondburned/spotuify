package layout

import (
	"testing"

	"github.com/Bios-Marcel/tview"
)

func TestLayout(t *testing.T) {
	tview.Borders.Horizontal = ' '

	l := NewLayout()

	Decoration.AddItem(
		tview.NewTextView().
			SetText("diamondburned").
			SetTextAlign(tview.AlignRight),
		0, 1, false,
	)

	Statusbar.SetTitle(" Polaris - Aimer - After Dark ")

	Media := tview.NewFlex().SetDirection(tview.FlexColumn)

	prev := tview.NewButton(" ⏮ ").SetSelectedFunc(playbackPrev)
	play := tview.NewButton(" ▶️ ").SetSelectedFunc(playbackPrev)
	next := tview.NewButton(" ⏭️ ").SetSelectedFunc(playbackPrev)

	Media.AddItem(prev, 0, 1, true)
	Media.AddItem(play, 0, 1, true)
	Media.AddItem(next, 0, 1, true)

	Statusbar.AddItem(
		Media, 1, 1, false,
	)

	Statusbar.AddItem(
		tview.NewTextView().
			SetText("[================                ]").
			SetTextAlign(tview.AlignCenter),
		0, 1, false,
	)

	if err := tview.NewApplication().SetRoot(l, true).Run(); err != nil {
		panic(err)
	}
}

func playbackPrev() {
	print("asdasd")
}
func playbackPlay() {}
func playbackNext() {}
