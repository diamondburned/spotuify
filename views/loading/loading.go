package loading

import (
	"github.com/Bios-Marcel/tview"
	"github.com/gdamore/tcell"
)

// Loading is the struct implementing the Primitive interface
type Loading struct {
	x, y          int
	width, height int
	focus         tview.Focusable

	visible bool

	s []rune
}

// NewLoading makes a new loading struct. This only
// does one line.
func NewLoading(loading string) *Loading {
	return &Loading{
		s:       []rune(loading),
		visible: true,
	}
}

// Draw draws
func (l *Loading) Draw(s tcell.Screen) bool {
	if l.width <= 0 || l.height <= 0 {
		return false
	}

	x, y := l.x+l.width/2-len(l.s)/2, l.y+l.height/2

	s.ShowCursor(x, y)

	for i := 0; i < len(l.s); i++ {
		s.SetContent(x+i, y, l.s[i], nil, 0)
	}

	s.HideCursor()

	return true
}

// SetVisible sets visibility like CSS.
func (l *Loading) SetVisible(v bool) {
	l.visible = v
}

// IsVisible returns visible
func (l *Loading) IsVisible() bool {
	return l.visible
}

// GetRect returns the rectangle dimensions
func (l *Loading) GetRect() (int, int, int, int) {
	return l.x, l.y, l.width, l.height
}

// SetRect sets the rectangle dimensions
func (l *Loading) SetRect(x, y, width, height int) {
	l.x = x
	l.y = y
	l.width = width
	l.height = height
}

// InputHandler sets no input handler, satisfying Primitive
func (l *Loading) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return nil
}

// Focus does nothing, really.
func (*Loading) Focus(delegate func(tview.Primitive)) {}

// Blur also does nothing.
func (*Loading) Blur() {}

// HasFocus always returns false, as you can't focus on this.
func (*Loading) HasFocus() bool {
	return false
}

// GetFocusable does whatever the fuck I have no idea
func (l *Loading) GetFocusable() tview.Focusable {
	return l.focus
}

func (*Loading) SetOnFocus(func()) {}
func (*Loading) SetOnBlur(func())  {}
