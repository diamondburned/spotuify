package main

import (
	"image"
	_ "image/jpeg"
	"net/http"
	"strings"

	"github.com/Bios-Marcel/tview"
	tviewsixel "gitlab.com/diamondburned/tview-sixel"
)

var (
	s 
)

func main() {
	r, err := http.Get("https://i.scdn.co/image/85a01c25a9ceeb806af3b77d5fab3b1e5ab547d2")
	if err != nil {
		panic(err)
	}

	defer r.Body.Close()

	img, _, err := image.Decode(r.Body)
	if err != nil {
		panic(err)
	}

	flex := tview.NewFlex()

	{
		tv := tview.NewTextView()
		tv.SetDynamicColors(true)

		_, _, _, h := tv.GetRect()

		tv.SetText(strings.Repeat("\n", (h/2)+3) +
			"[::b]Polaris[::-]\nAimer\nAfter Dark")

		tv.SetBackgroundColor(-1)
		tv.SetTextAlign(tview.AlignLeft)

		frame := tview.NewFrame(tv)
		frame.SetBorders(3, 3, 0, 0, 5, 5)

		flex.AddItem(frame, 0, 2, false)
	}

	{
		p, err := tviewsixel.NewPicture(img)
		if err != nil {
			panic(err)
		}

		defer p.Close()

		p.ChangeAlignment(tviewsixel.AlignMiddle)

		frame := tview.NewFrame(p)
		frame.SetBorders(4, 4, 0, 0, 4, 4)

		flex.AddItem(frame, 0, 3, true)
	}

	if err := tview.NewApplication().SetRoot(flex, true).Run(); err != nil {
		panic(err)
	}

}
